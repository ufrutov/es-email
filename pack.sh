#!/bin/bash

echo "Pack content into zip archive"

folder="es-email"

if [ -f "$folder.zip" ]; then
	echo "Remove previous archive"
	rm "$folder.zip"
fi

zip -r $folder dist -x $folder/node_modules/**\*

echo "Done! $folder.zip is ready"